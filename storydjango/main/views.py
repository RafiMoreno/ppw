from django.shortcuts import render
from django.http import HttpResponse


def home(request):
    return render(request, 'main/home.html')

def about(request):
    return render(request, 'main/about.html')

def story1(request):
    return render(request, 'main/Story1.html')

def story3(request):
    return render(request, 'main/Story3.html')

def story3gallery(request):
    return render(request, 'main/Story3Gallery.html')