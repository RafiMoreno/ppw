from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('about/', views.about, name='about'),
    path('story1/', views.story1, name='story1'),
    path('story3/', views.story3, name='story3'),
    path('story3gallery', views.story3gallery, name='story3gallery')
]
